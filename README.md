# Dataset doctor (WIP)

The idea is to have a place to edit and combine datasets

Maybe into the future it will be merged with audiostellar. For now is its own separate program, so its easy to test things out.

## ROADMAP

* Open data set and explore it (like explorer Mode)
* Open varius data sets. 
    * Have a window to select active data set
    * Grab a cluster or a sound and move it to another dataset
* Hide/Show clusters
* Save new datasets with hardcoded eps
