#include "Sounds.h"

Sounds::Sounds() {
    volumen = 0.5f;
    replaySound = false;
    useOriginalPositions = false;
    epsDbScan = 15;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();

}

void Sounds::loadSounds(ofxJSONElement jsonFile){

    if(!sounds.empty()){
        reset();
    }

    string soundsDir = jsonFile["audioFilesPath"].asString();

    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");
    
    initialWindowHeight = ofGetHeight();
    initialWindowWidth = ofGetWidth();
    setSpaceLimits(soundFiles);

    for(int i = 0; i<soundFiles.size() - 1; i++) {
        if(soundFiles[i] == "")continue;
        vector<string>lineElements = ofSplitString(soundFiles[i],",");
        
        int cluster = ofToInt(lineElements[TSV_FIELD_CLUSTER]);
        string name = lineElements[TSV_FIELD_NAME];
        
        ofVec3f position = soundToCamCoordinates( ofVec3f(
                    ofToFloat(lineElements[TSV_FIELD_X]),
                    ofToFloat(lineElements[TSV_FIELD_Y]),
                    ofToFloat(lineElements[TSV_FIELD_Z])) );

        string file = soundsDir + name;
        
        Sound *sound = new Sound(file, position, cluster);
        sound->id = idCount;
        idCount++;

        sounds.push_back(sound);
    }
    // Esto antes no estaba comentado pero
    doClustering();
}

void Sounds::reset(){
    //Resetea el tamaño

    sounds.clear();
    idCount = 0;
    backgroundAlpha = BG_ALPHA_MIN;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();
}

void Sounds::update() {
    for (int i = 0; i < sounds.size(); i++) {
        sounds[i]->update();
    }
}

void Sounds::draw() {
    ofFill();
    
    drawBackground();
    
    ofSetColor(255,255,255,255);
    
    //    for ( int i = 0 ; i < hulls.size() ; i++ ) {
    //        hulls[i].draw();
    //    }
    
    //ofSetCircleResolution(100);
    for (int i = 0; i < sounds.size(); i++) {
        if(!sounds[i]->getHide()) {
            sounds[i]->draw();
        }
        if ( showSoundFilenamesTooltip && sounds[i]->getHovered() ) {
            sounds[i]->drawGui();
        }
    }
}

void Sounds::drawBackground()
{
    ofPushMatrix();
    ofTranslate( -windowOriginalWidth*BG_TRANSLATION, -windowOriginalHeight*BG_TRANSLATION );
    ofSetColor(255,255,255,round(backgroundAlpha));
    fboBackground.draw(0,0);
    
    backgroundAlpha += BG_ALPHA_STEP * backgroundDirection * ofGetLastFrameTime() * 40;
    
    if ( backgroundAlpha >= BG_ALPHA_MAX ) {
        backgroundDirection = -1;
    } else if ( backgroundAlpha <= BG_ALPHA_MIN ) {
        backgroundDirection = 1;
    }
    ofPopMatrix();
}

void Sounds::generateBackground()
{
    fboBackground.allocate(windowOriginalWidth * 1.5, windowOriginalHeight * 1.5, GL_RGBA);
    
    fboBackground.begin();
    ofClear(25,0);
    ofPushMatrix();
    ofTranslate( windowOriginalWidth * BG_TRANSLATION, windowOriginalHeight * BG_TRANSLATION );
    
    float glowSize = SIZE_ORIGINAL * BG_GLOW_SIZE;
    
    for ( auto * sound : sounds ) {
        if ( sound->getCluster() < 0 ) {
            continue;
        }
        ofSetColor( ColorPaletteGenerator::getColor(sound->getCluster()) );
        //            ofDrawCircle(position.x, position.y, glowBigSize);
        Sound::imgGlow.draw(sound->position.x - glowSize / 2,
                            sound->position.y - glowSize / 2,
                            glowSize,
                            glowSize);
    }
    
    ofNoFill();
    ofSetColor(255,0,0);
    ofPopMatrix();
    //       ofDrawRectangle(1,1,fboBackground.getWidth()-2, fboBackground.getHeight()-1);
    fboBackground.end();
}

void Sounds::findConvexHulls() {
    ofxConvexHull convexHull;
    vector<ofVec3f> cluster;
    string strDebug = "clusterIDS: ";
    
    hulls.clear();
    
    for ( auto clusterId : clusterIds ) {
        strDebug += ofToString(clusterId) + ",";
        cluster.clear();
        for ( auto * sound : sounds ) {
            if ( sound->getCluster() == clusterId ) {
                cluster.push_back( sound->getPosition() );
            }
        }
        
        ofPolyline polyHull;
        if ( cluster.size() >= 3 ) {
            vector<ofPoint> hull = convexHull.getConvexHull( cluster );
            
            for ( int i = 0 ; i < hull.size() ; i++ ) {
                polyHull.addVertex( hull[i] );
            }
            polyHull.close();
            
            //        agrando desde el centro
            for( int i = 0 ; i < polyHull.size() ; i++ ) {
                auto &p = polyHull.getVertices()[i];
                
                p.x += polyHull.getNormalAtIndex(i).x * 15;
                p.y += polyHull.getNormalAtIndex(i).y * 15;
            }
        }
        
        hulls.push_back( polyHull );
    }
    //    ofLog() << strDebug;
    
    //    int currentCluster = 0;
    //    ofxConvexHull convexHull;
    //    vector<ofVec3f> cluster;
    
    //    hulls.clear();
    
    //    do {
    //        cluster.clear();
    //        for (int i = 0; i < sounds.size(); i++) {
    //            if ( sounds[i]->getCluster() == currentCluster ) {
    //                cluster.push_back( sounds[i]->getPosition() );
    //            }
    //        }
    
    //        if ( cluster.size() > 0 ) {
    //            vector<ofPoint> hull = convexHull.getConvexHull( cluster );
    //            ofPolyline polyHull;
    //            for ( int i = 0 ; i < hull.size() ; i++ ) {
    //                polyHull.addVertex( hull[i] );
    //            }
    
    //            polyHull.close();
    
    //            //agrando desde el centro
    ////            for( int i = 0 ; i < polyHull.size() ; i++ ) {
    ////                auto &p = polyHull.getVertices()[i];
    
    ////                p.x += polyHull.getNormalAtIndex(i).x * 15;
    ////                p.y += polyHull.getNormalAtIndex(i).y * 15;
    ////            }
    
    //            // polyHull = polyHull.getSmoothed(2, 0.5);
    //            hulls.push_back( polyHull );
    //        }
    
    //        currentCluster++;
    //    } while( cluster.size() > 0 );
}

void Sounds::updateSoundPosition(Sound * sound) {
    soundPosition * found = NULL;
    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        if ( soundPositions[i].sound == sound ) {
            found = &soundPositions[i];
            break;
        }
    }
    
    if ( found == NULL ) {
        found = new soundPosition;
        found->sound = sound;
        
        soundPositions.push_back(*found);
    }
    
    found->position.x = sound->position.x;
    found->position.y = sound->position.y;
}

void Sounds::mouseMoved(int x, int y, bool onGui) {
    if ( onGui ) {
        for ( auto * sound : sounds ) {
            sound->clusterIsHovered = true;
        }
        return;
    }
    
    if ( hoveredActivated ) {
        Sound * nearestSound = getNearestSound( ofVec2f(x,y) );
        allSoundsHoveredOff(nearestSound);
        if (nearestSound != NULL) {
            if ( !nearestSound->getHovered() ) {
                nearestSound->setHovered(true);
            }
        }
    }
    
    set<int> selectedClusters;
    int selectedCluster = -1;
    for ( int i = 0 ; i < hulls.size() ; i++ ) {
        if ( hulls[i].size() >= 3 && hulls[i].inside( x, y) ) {
            selectedClusters.insert(i);
        }
    }
    
    for ( auto cluster : selectedClusters ) {
        if ( selectedCluster == -1 ) {
            selectedCluster = cluster;
        } else {
            if ( hulls[cluster].getArea() < hulls[selectedCluster].getArea() ) {
                selectedCluster = cluster;
            }
        }
    }
    
    for ( auto * sound : sounds ) {
        if ( selectedCluster == -1 || sound->getCluster() == selectedCluster ) {
            sound->clusterIsHovered = true;
        } else {
            sound->clusterIsHovered = false;
        }
    }
    
    //    string strDebug = "selectedClusters: ";
    //    for ( auto cluster : selectedClusters ) {
    //        strDebug += ofToString(cluster) + ",";
    //    }
    //    ofLog() << strDebug;
    //    ofLog() << "selectedCluster: " << selectedCluster;
}

void Sounds::mouseDragged(ofVec2f p, int button) {
    if ( !useOriginalPositions && button == 2 ) {
        Sound * hoveredSound = getHoveredSound();
        
        if ( hoveredSound != NULL ) {
            hoveredSound->position = p;
            updateSoundPosition(hoveredSound);
        }
    }

    if ( button == 0 ) {
        Sound * nearestSound = getNearestSound(p);

        if ( nearestSound != NULL &&
             nearestSound != lastDraggedSound ) {
            playSound( nearestSound );
        }

        lastDraggedSound = nearestSound;
    }
}


void Sounds::allSoundsSelectedOff() {
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->selected = false;
    }
}
void Sounds::allSoundsHoveredOff(Sound * except) {
    for(int i = 0; i < sounds.size(); i++) {
        if ( except != NULL && sounds[i] == except ) {
            continue;
        }
        if ( sounds[i]->getHovered() ) {
            sounds[i]->setHovered(false);
        }
    }
}

Sound * Sounds::playSoundAt(ofVec2f playerPosition) {
    Sound * nearestSound = getNearestSound(playerPosition);
    
    if ( nearestSound != NULL ) {
        playSound( nearestSound );
    }

    return nearestSound;
}

Sound * Sounds::getNearestSound(ofVec2f position) {
    if ( sounds.size() == 0 ) {
        return NULL;
    }
    float minDistance = sounds[0]->getPosition().squareDistance( position );
    int minDistanceIndex = -1;
    
    float currentDistance = 0;
    
    for(int i = 1; i < sounds.size(); i++) {
        if ( sounds[i]->getHide() ) {
            continue;
        }
        
        currentDistance = sounds[i]->getPosition().squareDistance( position );
        
        if ( currentDistance > pow(sounds[i]->size * 3,2) ) {
            continue;
        }
        
        if ( currentDistance < minDistance ) {
            minDistance = currentDistance;
            minDistanceIndex = i;
        }
    }
    
    if ( minDistanceIndex != -1 ) {
        return sounds[minDistanceIndex];
    } else {
        return NULL;
    }
}

Sound * Sounds::getHoveredSound() {
    for(int i = 0; i < sounds.size(); i++) {
        if ( sounds[i]->hovered ) {
            return sounds[i];
        }
    }
    
    return NULL;
}

void Sounds::playSound(Sound * sound , float volumeMult) {
    vector<string> pathArr = ofSplitString(sound->getFileName(),"/");
    currentSound = pathArr[pathArr.size() - 1];
    // currentSound = sound->getFileName();
    
    //si no está cargado
    if(!sound->isLoaded()) {
        ofFile soundFile( sound->getFileName() );
        if ( !soundFile.exists() ) {
            ofLog() << "Warning: Inexistent file " << sound->getFileName() << endl;
            return;
        }

        //te fijas si ya cargaste más que el limite
        if(nLoadedSounds >= MAX_LOADED_SOUNDS) {
            //empezas a ubicarlos del principio
            nLoadedSounds = 0;
        }
        //si el elemento actual no es NULL
        if(loadedSounds[nLoadedSounds] != NULL) {
            loadedSounds[nLoadedSounds]->unload();
            loadedSounds[nLoadedSounds] = NULL;
        }
        
        sound->load(sound->getFileName());
        sound->setMultiPlay(false);

        
        loadedSounds[nLoadedSounds] = sound;
        nLoadedSounds++;
    }
    
    sound->setVolume(getVolume() * volumeMult);
    
    //esto me parece que habría que chequearlo desde más arriba
    if( !sound->isPlaying() || replaySound ) {
        sound->play();
        lastPlayedSound = sound;
    }
    
    setFilenameLabel( getCurrentSound() );
}

void Sounds::playSound(int id)
{
    playSound( getSoundById(id) );
}

void Sounds::setSpaceLimits(vector<string> soundsCoords) {
    ofVec3f max(-99999999, -99999999, -99999999);
    ofVec3f min(99999999, 99999999, 99999999);
    // spaceLimits = {ofVec2f(0,0),ofVec2f(1500,1500)};
    spaceLimits[0] = max;
    spaceLimits[1] = min;
    
    
    for(int i = 0; i< soundsCoords.size() - 1; i++) {
        if(soundsCoords[i] == "")continue;
        auto lineElements = ofSplitString(soundsCoords[i],",");
        
        ofVec3f position(ofToFloat(lineElements[TSV_FIELD_X]), ofToFloat(lineElements[TSV_FIELD_Y]), ofToFloat(lineElements[TSV_FIELD_Z]));
        
        spaceLimits[0].x = Utils::getMaxVal(position.x, spaceLimits[0].x);
        spaceLimits[0].y = Utils::getMaxVal(position.y, spaceLimits[0].y);
        spaceLimits[0].z = Utils::getMaxVal(position.z, spaceLimits[0].z);
        
        spaceLimits[1].x = Utils::getMinVal(position.x, spaceLimits[1].x);
        spaceLimits[1].y = Utils::getMinVal(position.y, spaceLimits[1].y);
        spaceLimits[1].z = Utils::getMinVal(position.z, spaceLimits[1].z);
        
    }
    
}

void Sounds::drawGui(){
    ImGui::Checkbox("Replay Sound", &replaySound);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_REPLAY);

    if(ImGui::Checkbox("Original Positions", &useOriginalPositions)){
            setUseOriginalPositions();
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_ORIGINAL_POSITIONS);

    ImGui::Separator();

   //CLUSTERING OPTIONS

    ImGui::SliderInt("Cluster Eps", &epsDbScan, 5, 100);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_EPS);

    if ( ImGui::IsItemHovered() && ImGui::IsMouseReleased(0) ) {
       doClustering();
    }
    if(ImGui::Button("Export files")){
       exportFiles();
    }
    
    ImGui::Separator();
    
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_EXPORT_FILES);
}

void Sounds::setUseOriginalPositions() {
    for(int i = 0; i<soundPositions.size(); i++) {
        if (useOriginalPositions) {
            soundPositions[i].sound->useOriginalPosition();
        } else {
            soundPositions[i].sound->position = soundPositions[i].position;
        }
    }
}

void Sounds::onClusterToggle(int clusterIdx) {
    for(int i = 0; i<sounds.size(); i++) {
        if(sounds[i]->getCluster() == clusterIdx) {
            sounds[i]->toggleHide();
        }
        
    }
}

void Sounds::doClustering()
{
    DBScan dbscan( epsDbScan, 5, getSoundsAsPoints());
    dbscan.run();
    setDBScanClusters(dbscan.points);
    findConvexHulls();
    generateBackground();
}

Sound * Sounds::getSoundByFilename(string filename) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getFileName() == filename ) {
            return sounds[i];
        }
    }
    return NULL;
}
Sound * Sounds::getSoundById(int id) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->id == id ) {
            return sounds[i];
        }
    }
    return NULL;
}

vector<Sound *> Sounds::getSounds()
{
    return sounds;
}

vector<ofPoint> Sounds::getSoundsAsPoints()
{
    vector<ofPoint> soundsAsPoints;
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        soundsAsPoints.push_back( sounds[i]->getPosition() );
    }
    
    return soundsAsPoints;
}

void Sounds::setDBScanClusters(vector<dbscanPoint> points)
{
    clusterIds.clear();
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( points[i].cluster );
        
        if ( points[i].cluster >= 0 ) {
            clusterIds.insert( points[i].cluster );
            //Para saber si cada cluster se está mostrando. Asumo que todos los clusters empiezan siendo mostrados...
            clustersShow.push_back(true);
        }
    }
}


float Sounds::getVolume() {
    return volumen;
}
string Sounds::getCurrentSound() {
    return currentSound;
}

void Sounds::setFilenameLabel(string fileName){
    currentSoundLabel = fileName;
}

bool Sounds::getUseOriginalPositions(){
  return useOriginalPositions;
}

bool Sounds::getReplaySound(){
  return replaySound;
}

bool Sounds::getShowSoundFilenames(){
    return showSoundFilenamesTooltip;
}

set<int> Sounds::getClusterIds(){
    return clusterIds;
}

ofVec3f Sounds::camToSoundCoordinates(ofVec2f camCoordinates) {
    return camToSoundCoordinates( ofVec3f(camCoordinates.x, camCoordinates.y, 0) );
}

ofVec3f Sounds::camToSoundCoordinates(ofVec3f camCoordinates){
    ofVec3f r;

    r.x = ofMap(camCoordinates.x,
                0 + spacePadding,
                initialWindowWidth - spacePadding,
                spaceLimits[1].x,
                spaceLimits[0].x);


    r.y = ofMap(camCoordinates.y,
                0 + spacePadding,
                initialWindowHeight - spacePadding,
                spaceLimits[1].y,
                spaceLimits[0].y);

    r.z = ofMap(camCoordinates.z,
                   0.5,1,
                   spaceLimits[1].z,
                   spaceLimits[0].z);

    return r;
}

ofVec3f Sounds::soundToCamCoordinates(ofVec3f soundCoordinates){
    ofVec3f r;

    r.x = ofMap(soundCoordinates.x,
                       spaceLimits[1].x,
                       spaceLimits[0].x,
                       0 + spacePadding,
                       initialWindowWidth - spacePadding);

    r.y = ofMap(soundCoordinates.y,
                       spaceLimits[1].y,
                       spaceLimits[0].y,
                       0 + spacePadding,
                       initialWindowHeight - spacePadding);

    r.z = ofMap(soundCoordinates.z,
                       spaceLimits[1].z,
                       spaceLimits[0].z,
                       0.5,1);

    return r;
}

void Sounds::setUseOrigPos(bool v){
  useOriginalPositions = v;
}

void Sounds::setReplaySound(bool v){
  replaySound = v;
}

void Sounds::setShowSoundFilenames(bool v){
  showSoundFilenamesTooltip = v;
}

Json::Value Sounds::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value overridedPositions = Json::Value( Json::arrayValue );
    
    for ( auto sPos : soundPositions ) {
        Json::Value soundPosition = Json::Value( Json::objectValue );
        soundPosition["id"] = sPos.sound->id;
        soundPosition["position"] = Json::Value( Json::arrayValue );
        soundPosition["position"].append( sPos.position.x );
        soundPosition["position"].append( sPos.position.y );
        
        overridedPositions.append(soundPosition);
    }
    
    root["overridedPositions"] = overridedPositions;
    root["DBScan_EPS"] = epsDbScan;
    return root;
}

void Sounds::load( Json::Value jsonData ) {
    Json::Value overridedPositions = jsonData["overridedPositions"];
    for ( int i = 0 ; i < overridedPositions.size() ; i++ ) {
        // Sound * sound = getSoundById( overridedPositions[i]["id"] )
        soundPosition newSoundPosition;
        newSoundPosition.sound = getSoundById( overridedPositions[i]["id"].asInt() );
        
        if ( newSoundPosition.sound == NULL ) {
            ofLogWarning("Sound ID not found when loading session");
            continue;
        }
        
        newSoundPosition.position.x = overridedPositions[i]["position"][0].asInt();
        newSoundPosition.position.y = overridedPositions[i]["position"][1].asInt();
        
        soundPositions.push_back(newSoundPosition);
    }
    
    if ( jsonData["DBScan_EPS"].isInt() ) {
        epsDbScan = jsonData["DBScan_EPS"].asInt();
    }
    doClustering();
}

string Sounds::selectFolder() {
    
    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
    
    string path;
    
    if(dialogResult.bSuccess) {
        path = dialogResult.getPath();
    }
    
    return path;
}

void Sounds::exportFiles() {
    
    string selectedFolderPath = selectFolder();
    
    if (selectedFolderPath.size() > 0) {
        
        selectedFolderPath = ofFilePath::addTrailingSlash(selectedFolderPath);
        
        for (std::set<int>::iterator it=clusterIds.begin(); it!=clusterIds.end(); ++it) {
            
            string clusterId = to_string(*it);
            string clusterFolderPath = selectedFolderPath + clusterId;
            
            ofDirectory dir(clusterFolderPath);
            if(!dir.exists()){
                dir.create(true);
            }
        }
        
        for(int i = 0; i < sounds.size(); i++) {
            
            if (sounds[i]->getCluster() >= 0 ) {
                
                string cluster = to_string( sounds[i]->getCluster() );
                string sourcePath = sounds[i]->getFileName();
                string destinationPath = selectedFolderPath + cluster;
                
                ofFile::copyFromTo(sourcePath, destinationPath, true, false);
                
            }
        }
    }
}
