#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ColorPaletteGenerator.h"
#include "Sound.h"
#include "ofxJSON.h"
#include "ofxConvexHull.h"
#include "DBScan.h"
#include "ofxImGui.h"
#include "Tooltip.h"

struct soundPosition {
  Sound * sound;
  ofVec2f position;
};

class Sounds {
private:
#define MAX_LOADED_SOUNDS 100

    vector<Sound*> sounds;

    ofBuffer tsvFile;

    bool replaySound;
    bool useOriginalPositions;

    string currentSoundLabel = "";

    int epsDbScan;

    void setSpaceLimits(vector<string> soundsCoords);
    ofVec3f spaceLimits[2];
    int initialWindowWidth = 0;
    int initialWindowHeight = 0;
    const int spacePadding = 30;

    set<int> clusterIds;
    vector<bool> clustersShow;

    bool skipFileHeader = true;//salteate la primera línea al leer el .tsv


    int idCount = 0;

#define BG_TRANSLATION 0.3
#define BG_ALPHA_MIN 30
#define BG_ALPHA_MAX 50
#define BG_ALPHA_STEP 0.3
#define BG_GLOW_SIZE 50
    
    int windowOriginalWidth;
    int windowOriginalHeight;
    ofFbo fboBackground;
    float backgroundAlpha = BG_ALPHA_MIN;
    int backgroundDirection = 1;

    //overrided Positions
    vector<soundPosition> soundPositions;
    void updateSoundPosition(Sound * sound);

    void findConvexHulls();
    vector<ofPolyline> hulls;
public:
    Sounds();

    float volumen;
    // Cada modo debería setear si necesita o no que se checkee hover
    bool hoveredActivated = false;
    Sound * lastPlayedSound = nullptr;
    Sound * lastDraggedSound = nullptr;
    bool showSoundFilenamesTooltip = false;

    //MAIN
    void loadSounds(ofxJSONElement jsonFile);
    void update();
    void draw();
    void reset();

    //DRAW
    void drawBackground();
    void generateBackground();
    void drawGui();

    //SESSION
    Json::Value save();
    void load( Json::Value jsonData );

    //CLUSTERING
    void onClusterToggle(int clusterIdx);
    void doClustering();

    //MOUSE
    void mouseMoved(int x, int y, bool onGui);
    void mouseDragged(ofVec2f p, int button);

    //PLAY
    Sound * playSoundAt(ofVec2f position);
    void playSound(Sound * sound, float volumeMult = 1.0f);
    void playSound(int id);
    Sound * getNearestSound(ofVec2f position);

    void allSoundsSelectedOff();
    void allSoundsHoveredOff(Sound * except = NULL);
    
    //No se encarga de setear el valor, sino que hace toda la movida de reubicar los audios
    void setUseOriginalPositions();

    //SETTERS
    void setFilenameLabel(string fileName);
    void setDBScanClusters( vector<dbscanPoint> points );
    void setReplaySound(bool v);
    void setUseOrigPos(bool v);
    void setShowSoundFilenames(bool v);
      

    //GETTERS
    float getVolume();
    string getCurrentSound();
    Sound * getHoveredSound();
    Sound * getSoundByFilename(string filename);
    Sound * getSoundById(int id);
    vector<Sound*> getSounds();
    set<int> getClusterIds();
    vector<ofPoint> getSoundsAsPoints();
    bool getReplaySound();
    bool getUseOriginalPositions();
    bool getShowSoundFilenames();

    //COORDINATES
    ofVec3f camToSoundCoordinates(ofVec3f camCoordinates);
    ofVec3f camToSoundCoordinates(ofVec2f camCoordinates);
    ofVec3f soundToCamCoordinates(ofVec3f soundCoordinates);


    string currentSound = "";
    // bool enableHovering = false; Esto debería ser asi

    int nLoadedSounds = 0;
    Sound *loadedSounds[MAX_LOADED_SOUNDS] = {NULL};//llevar la cuenta de los sonidos cargados
    
    string selectFolder();
    void exportFiles();
};
