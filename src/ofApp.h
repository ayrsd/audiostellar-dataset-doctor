#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Session.h"
#include "Utils.h"
#include "Sound.h"
#include "Utils/Gui.h"
#include "Modes.h"
#include "CamZoomAndPan.h"
#include "DBScan.h"
#include "ColorPaletteGenerator.h"
#include "AudioDimensionalityReduction.h"



class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();
    void exit();


    void drawFps();

    void keyPressed(int key);
    void keyReleased(int key);
    void mousePressed(int x, int y, int button);
    void mouseScrolled(int x, int y, float scrollX, float scrollY);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseMoved(int x, int y);

    char cwd[1024];
    vector<string> arguments;

    Session * session = NULL;
    Sounds * sounds = NULL;
    Modes * modes = NULL;
    Gui * gui = NULL;

    ofxJSONElement jsonFile;

    CamZoomAndPan cam;
    bool useCam = true;

};
