#include "SessionManager.h"


SessionManager::SessionManager(Modes * _modes, Sounds * _sounds){

    modes = _modes;
    sounds = _sounds;
    
    //SETTINGS FILE
    if(!settingsFileExist()){
      ofLogNotice("HI", "No settings file. Creating one...");
      createSettingsFile();
    }

    //This loads the json only
    loadSettingsFile();
    //this mutates the variables acording to the loaded file
    loadIntoSettings();

}

void SessionManager::drawGui(){

    if(ImGui::Button("Save session")){
        saveSession();
    }

    if(ImGui::Button("Load")){
        loadSession();
    }

}

void SessionManager::loadSession() {
   ofxJSONElement json = loadJsonGui();
   if(json != Json::nullValue){
        loadSessionFromJSON(json);
   }

}

void SessionManager::loadSession(vector <string> args){
   ofxJSONElement json = loadJsonTerminal(args);
   loadSessionFromJSON(json);
}

void SessionManager::loadSession(string path){
    if ( ofFile::doesFileExist(path) ) {
        ofxJSONElement json = loadJson(path);
        loadSessionFromJSON(json);
    }
}

void SessionManager::loadSessionFromJSON(ofxJSONElement file){

    //ESTO CARGA LOS SONIDOS
    sounds->loadSounds(file);

    Json::Value jsonModes = file["modes"];

    for ( int i = 0 ; i < modes->modes.size() ; i++ ) {
         modes->modes[i]->reset();
    }

    if ( jsonModes != Json::nullValue ) {
        for ( int i = 0 ; i < modes->modes.size() ; i++ ) {
            modes->modes[i]->load( jsonModes[ modes->modeNames[i] ] );
        }
    }

    //ESTO CARGA CONFIGURACIONES RELACIONADAS A LOS SONIDOS
    sounds->load(file["sounds"]);
    setWindowTitleWithSessionName();
}


ofxJSONElement SessionManager::loadJsonGui(){

    ofFileDialogResult selection =
        ofSystemLoadDialog("Select JSON file");

    ofxJSONElement file;

    if(selection.bSuccess) {
        string filename = selection.getPath();
        file = loadJson(filename);
    }

    return file;
}

ofxJSONElement SessionManager::loadJsonTerminal(vector<string> args) {

    ofxJSONElement file;
    bool osx = false;
    #ifdef TARGET_OSX
    osx = true;
    #endif

    if(args.size() > 1 && !osx) {
        string filename = args.at(1);
        file = loadJson(filename);
    } else {
        ofLogNotice("ERROR", "Bad Arguments or mac not supported :/");
        ofExit(1);
    }

    return file;
}

ofxJSONElement SessionManager::loadJson(string path){
    string ext = ofFilePath::getFileExt(path);

    ofxJSONElement file;

    if(ext != "json"){
      ofLogNotice("OOPS", "This is not a JSON file");
      Gui::getInstance()->showMessage("This is not a json file", "Error", true);
    } else {
      ofxJSONElement tempFile;
      tempFile = parseJson(path);

      //Check if minimum fields exist
      Json::Value soundFilesPath = tempFile["audioFilesPath"];
      Json::Value map = tempFile["tsv"];

      //

      if(soundFilesPath == Json::nullValue || map == Json::nullValue){
          ofLogNotice("OOPS", "not a valid sound map");
          Gui::getInstance()->showMessage("Not a valid sound map", "Error", true);
      } else {
          file = tempFile;
          saveToRecentProject(path);
          sessionStarted = true;
      }
    }

    return file;

}

ofxJSONElement SessionManager::parseJson(string path){
    ofxJSONElement file;
    bool loadedJson = file.open(path);

    //Seteo las dos globales de la clase
    jsonFilename = path;
    jsonFile = file;

    if(!loadedJson) {
        ofLogNotice("ERROR", "problemas al cargar el json");
        ofExit(1);
    }

    file["audioFilesPath"] = Utils::resolvePath
                                 (path, file["audioFilesPath"].asString());
    return file;
}
void SessionManager::loadSettingsFile(){

  ofxJSONElement file;
  bool loaded = file.open(ofToDataPath(settingsFilename));

  if(!loaded){
    ofLogNotice("ERROR", "error loading settings json file");
  }
  
  Json::Value recentProjects = file["recentProjects"];
  Json::Value replaySound = file["replaySound"];
  Json::Value useOrigPos = file["useOriginalPositions"];
  Json::Value showSoundFilenames = file["showSoundFilenames"];

  if(recentProjects == Json::nullValue ||
      replaySound == Json::nullValue ||
      useOrigPos == Json::nullValue ||
      showSoundFilenames == Json::nullValue){
    ofLogNotice("ERROR", "corrupted settings file. Deleting and creating new...");
    ofFile::removeFile(ofToDataPath(settingsFilename));
    createSettingsFile();
  }else{
    //seteamos la global
    settingsFile = file;
  }

}

void SessionManager::loadIntoSettings(){
    //recentProjects
    recentProjects = loadRecentProjects();

    //cargá el último
    if(areAnyRecentProjects(recentProjects)){
        loadSession(recentProjects[recentProjects.size() - 1]);
    }

    
    //ReplaySound
    bool rSound = settingsFile["replaySound"].asInt() == 1 ? true : false;
    sounds->setReplaySound(rSound);
    
    //Use original Positions
    bool useOrigP = settingsFile["useOriginalPositions"].asInt() == 1 ? true : false;
    sounds->setUseOrigPos(useOrigP);

    //Show sound filenames
    bool showSoundFilenames = settingsFile["showSoundFilenames"].asInt() == 1 ? true : false;
    sounds->setShowSoundFilenames(showSoundFilenames);
}

void SessionManager::saveSettingsFile(){
  Json::Value root = Json::Value(Json::objectValue);

   //Recent Projects
   Json::Value recent = Json::Value(Json::arrayValue);
   for(unsigned int i = 0 ; i < recentProjects.size(); i++){
       recent[i] = recentProjects[i];
   }

   root["recentProjects"] = recent;


   //ReplaySound
   int replaySound = sounds->getReplaySound() ? 1 : 0;
   root["replaySound"] = replaySound;

   //Use original positions
   int useOrigPos = sounds->getUseOriginalPositions() ? 1 : 0;
   root["useOriginalPositions"] = useOrigPos;

   //Show sound filenames
   int showSoundFilenames = sounds->getShowSoundFilenames() ? 1 : 0;
   root["showSoundFilenames"] = showSoundFilenames;

   settingsFile = root;
   settingsFile.save(ofToDataPath(settingsFilename));
}
bool SessionManager::settingsFileExist(){
  return ofFile::doesFileExist(settingsFilename);
}

void SessionManager::createSettingsFile(){
  Json::Value value = Json::Value (Json::objectValue);

  value["recentProjects"] = Json::Value(Json::arrayValue);
  value["lastMidiDevice"] = "";
  value["replaySound"] = Json::Value(Json::intValue);
  value["useOriginalPositions"] = Json::Value(Json::intValue);
  value["showSoundFilenames"] = Json::Value(Json::intValue);

  //seteamos la global
  settingsFile = value;
  settingsFile.save(ofToDataPath(settingsFilename));
}

vector<string> SessionManager::loadRecentProjects(){
    vector<string> r;
    Json::Value recent = Json::Value(Json::arrayValue);

    recent = settingsFile["recentProjects"];
    for(unsigned int i = 0; i < recent.size(); i ++){
      if(recent[i] != Json::nullValue){
        r.push_back(recent[i].asString());
      }
    }

    //std::reverse(recent.begin(),recent.end());
    return r;

}

void SessionManager::saveToRecentProject(string path){
    //Si no hay recientes
    if(recentProjects.size() == 0){
        recentProjects.push_back(path);
    } else {

        for(unsigned int i = 0 ; i < recentProjects.size(); i++){
            if(path == recentProjects[i]){
                recentProjects.erase( recentProjects.begin()+i );
                break;
            }
        }

        recentProjects.push_back(path);
    }
}


void SessionManager::saveSession() {
    ofLog() << "Saving...";

    /****
        Sounds
    ****/

    Json::Value jsonSounds = jsonFile["sounds"];

    if ( jsonSounds == Json::nullValue ) {
        jsonFile["sounds"] = Json::Value( Json::objectValue );
    }

    jsonFile["sounds"] = sounds->save();


    /****
        MODES
    ****/

    Json::Value jsonModes = jsonFile["modes"];

    if ( jsonModes == Json::nullValue ) {
        jsonFile["modes"] = Json::Value( Json::objectValue );
    }

    for ( int i = 0 ; i < modes->modeNames.size() ; i++ ) {
        jsonFile["modes"][ modes->modeNames[i] ] = modes->modes[i]->save();
    }

    ofLog() << "Done saving";
}

void SessionManager::saveAsNewSession(){
  ofFileDialogResult result = ofSystemSaveDialog(
              ofFilePath::getEnclosingDirectory(jsonFilename) + "/new_session.json", "Save");

  if(result.bSuccess){
    string path = result.getPath();
    
    if(!ofIsStringInString(path, ".json")){
        path = path + ".json";
    }

    //Seteando el nombre de vuelta --> OJO
    jsonFilename = path;
    saveSession();
    setWindowTitleWithSessionName();
    saveToRecentProject(jsonFilename);
  }
}


vector <string> SessionManager::getRecentProjects(){
        vector<string> invertedRecent;
        for(int i = recentProjects.size() - 1; i >= 0; i --){
                invertedRecent.push_back(recentProjects[i]);
        }
        return invertedRecent;
}

bool SessionManager::getSessionStarted(){
        return sessionStarted;
}

bool SessionManager::areAnyRecentProjects(vector<string> recentProjects){
   return recentProjects.size() > 0 && recentProjects[recentProjects.size() - 1] != "";
}

//On Exit
void SessionManager::exit(){
   saveSettingsFile();
   ofLogNotice("Written settings files");

}

void SessionManager::setWindowTitleWithSessionName(){
    ofSetWindowTitle("AudioStellar // " + ofFilePath::getFileName( jsonFilename ) );
}
