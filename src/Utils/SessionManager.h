#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "Modes/Modes.h"
#include "Sound/Sounds.h"
#include "ofxImGui.h"
#include "Gui.h"


class SessionManager {

private:

    Modes * modes;
    Sounds * sounds;

    bool sessionStarted = false;

    //Stellar file
    const string settingsFilename = "stellar.json";
    ofxJSONElement settingsFile;
    bool settingsFileExist();
    void createSettingsFile();
    void loadSettingsFile();
    void loadIntoSettings();
    void saveSettingsFile();
   

    //RECENT PROJECTS
    ofFile recentProjectsFile;
    vector<string> recentProjects;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);


    //LOAD JSON
    string jsonFilename = "";
    ofxJSONElement jsonFile;

    ofxJSONElement loadJsonTerminal(vector<string> args);
    ofxJSONElement loadJsonGui();
    ofxJSONElement loadJson(string path);

    void loadSessionFromJSON(ofxJSONElement jsonFile);
    ofxJSONElement parseJson(string jsonPath);

    void setWindowTitleWithSessionName();

public:

    SessionManager(Modes * _modes, Sounds * _sounds);


    void loadSession();//De gui
    void loadSession(vector<string> args);//De terminal
    void loadSession(string path);//de recent

    void saveSession();
    void saveAsNewSession();
    void drawGui();
    void exit();

    vector<string> getRecentProjects();
    bool areAnyRecentProjects(vector<string> recentProjects);
    bool getSessionStarted();
};
