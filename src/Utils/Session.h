#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "Gui.h"
#include "Sounds.h"

class Session{

private:
    const string STRING_ERROR = "ERROR";

    struct datasetResult{
        bool success;
        string status;
        ofxJSONElement data;
    };

    Sounds * sounds = NULL;

    bool datasetLoaded = false;
    vector<ofxJSONElement> datasets;

    string userSelectJson();
    datasetResult validateDataset(string datasetPath);


public:
    Session(Sounds * sounds);
    void drawGui();

    void loadDataset();

    //Getters
    bool getDatasetLoaded();

};
