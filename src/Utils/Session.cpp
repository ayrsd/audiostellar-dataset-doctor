#include "Session.h"

Session::Session(Sounds * sounds){
        this->sounds = sounds;
}

void Session::loadDataset(){
    string datasetPath = userSelectJson();
    datasetResult loadingDatasetResult = validateDataset(datasetPath);

    if(!loadingDatasetResult.success){
        Gui::getInstance()->showMessage(
                    loadingDatasetResult.status.c_str(),
                    STRING_ERROR.c_str()
                    );
    }else{
       datasetLoaded = true;
       ofxJSONElement file = loadingDatasetResult.data;
       //Safety measure with paths
       file["audioFilesPath"] = Utils::resolvePath
                                    (datasetPath,
                                     file["audioFilesPath"].asString());
       datasets.push_back(file);
       sounds->loadSounds(file);
    }
}

string Session::userSelectJson(){
        string filename;
        ofFileDialogResult selection = ofSystemLoadDialog("Select JSON file");

        if(selection.bSuccess){
            filename = selection.getPath();
        }
        return filename;

}

Session::datasetResult Session::validateDataset(string datasetPath){
   datasetResult res;
   string extension = ofFilePath::getFileExt(datasetPath);

   //Check extension
   if(extension != "json"){
       res.success = false;
       res.status = "This is not a JSON file";
       return res;
   }

   //Check if valid json
   bool successLoading = res.data.open(datasetPath);
   if(!successLoading){
      res.success = false;
      res.status = "Error loading JSON";
      return res;
   }


   //Check if minimun fields exist
   Json::Value soundFilesPath = res.data["audioFilesPath"];
   Json::Value map = res.data["tsv"];

   if(soundFilesPath == Json::nullValue || map == Json::nullValue){
       res.success = false;
       res.status = "Not a valid sound map";
       return res;
   }

   res.success = true;
   res.status = "OK";
   return res;

}

bool Session::getDatasetLoaded(){
    return datasetLoaded;
}
