#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Sounds.h"
#include "Modes.h"
#include "Session.h"
#include "AudioDimensionalityReduction.h"
#include "GuiTheme.h"
#include "ofxJSON.h"
#include "Tooltip.h"

#define WINDOW_TOOLS_WIDTH 350
#define WINDOW_TOOLS_HEIGHT 600

class Session;

class Gui {

private:
    static Gui* instance;
    Gui(Sounds * sounds, Modes * modes, Session * session);

    AudioDimensionalityReduction adr;
    bool drawGui = true;

    Session * session;
    Sounds * sounds;
    Modes * modes;
    Tooltip * tooltip;

    ofxJSONElement jsonFile;

    bool isToolsWindowHovered = false;
    bool isClusterWindowHovered = false;
    bool isMainMenuHovered = false;
    bool isModeSelectorHovered = false;
    bool isAboutScreenHovered = false;
    bool isWelcomeScreenHovered = false;
    bool isDimReductScreenHovered = false;
    bool isProcessingFilesWindowHovered = false;
    bool isMessageWindowHovered = false;

    bool isWelcomeScreenOpen = true;
    bool isAboutScreenOpen = true;
    bool isDimReductScreenOpen = true;
    
    bool haveToDrawAboutScreen = false;
    bool haveToDrawDimReductScreen = false;
    bool haveToDrawMessage = false;
    bool haveToDrawMessageCloseButton = false;
    bool haveToDrawFPS = false;
    bool haveToHideCursor = false;
    bool toggleFullscreen = false;
    bool isFullscreen = false;

    //Message window
    string messageTitle = "";
    string messageDescription = "";

//    This should be implemented in 0.9.1
//
//    const char* sample_rate_items[4] = { "11025", "22050", "44100", "48000" };
//    int sample_rate_values[4] = { 11025, 22050, 44100, 48000 };
//    int sample_rate_item_current = 1;
    
//    const char* window_size_items[5] = { "512", "1024", "2048", "4096", "8192" };
//    int window_size_values[5] = { 512, 1024, 2048, 4096, 8192 };
//    int window_size_item_current = 2;
    
//    const char* hop_size_items[5] = { "Window size ", "Window size / 2", "Window size / 4", "Window size / 8", "Window size / 16" };
//    int hop_size_values[5] = { 1, 2, 4, 8, 16 };
//    int hop_size_item_current = 2;

    const char* features_items[2] = { "stft", "mfcc" };
    int features_item_current = 0;
    
    const string welcomeScreenText =
            "This tool will help you create new datasets, manipulate existing ones, and combine multiple datasets into one."
            "\n\n"
            "You can start by loading an already generated dataset by clicking in the 'Load Dataset' button"
            "Or you can create your own from scratch by clicking in 'New Dataset'"
            "\n\n"
            "You can find all of our datasets at https://gitlab.com/ayrsd/audiostellar"
            "\n\n";

    void newSession();
    bool isProcessingFiles = false;


    bool drawWelcomeScreen();
    void drawMainMenu();
    void drawToolsWindow();
    void drawClusterWindow();
    void drawAboutScreen();

    void drawFPS();
    void drawMessage();

    void drawDimReductScreen();
    void drawProcessingFilesWindow();



    ofTrueTypeFont font;
public:

    static Gui* getInstance();
    static Gui* getInstance(Sounds * sounds, Modes * modes, Session * session);

    ofxImGui::Gui gui;



    void draw();
    void keyPressed(int key);

    bool isMouseHoveringGUI();

    void showMessage(string description, string title = "", bool showCloseButton = true);
    void hideMessage();

};
