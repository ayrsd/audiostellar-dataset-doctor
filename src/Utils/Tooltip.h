#pragma once

#include "ofMain.h"
#include "ofxImGui.h"



class Tooltip {
    
public:
    
    struct tooltip {
        string title;
        string content;
    };
    
    static bool itemHovered;
    static string tooltipContent;
    static string tooltipTitle;
    
    static void setTooltip(tooltip tooltip);
    static void drawGui();
    
    static tooltip SOUNDS_VOLUME;
    static tooltip SOUNDS_REPLAY;
    static tooltip SOUNDS_ORIGINAL_POSITIONS;
    static tooltip SOUNDS_PLAY_ON_HOVER;
    static tooltip CLUSTERS_EPS;
    static tooltip CLUSTERS;
    static tooltip CLUSTERS_EXPORT_FILES;
    static tooltip SESSION_SAVE;
    static tooltip MODE_EXPLORER;
    static tooltip MODE_PARTICLE;
    static tooltip MODE_SEQUENCE;
    static tooltip PARTICLE_AGE;
    static tooltip PARTICLE_RANDOMIZE_EMITTER;
    static tooltip SIMPLE_MODEL;
    static tooltip SIMPLE_X_ACCELERATION;
    static tooltip SIMPLE_Y_ACCELERATION;
    static tooltip SWARM_MODEL;
    static tooltip SWARM_X_ACCELERATION;
    static tooltip SWARM_Y_ACCELERATION;
    static tooltip SPREAD_AMOUNT;
    static tooltip EXPLOSION_MODEL;
    static tooltip EXPLOSION_SPEED;
    static tooltip EXPLOSION_DENSITY;
    static tooltip ATTRACTOR_GRAVITY;
    static tooltip ATTRACTOR_MASS;
    static tooltip SEQUENCE_BPM;
    static tooltip SEQUENCE_ACTIVE;
    static tooltip SEQUENCE_VOLUME;
    static tooltip SEQUENCE_OFFSET;
    static tooltip SEQUENCE_PROBABILITY;
    static tooltip SEQUENCE_BARS;
    static tooltip SEQUENCE_CLEAR;
    static tooltip MIDI_DEVICES;
    static tooltip MIDI_LEARN;
    static tooltip MIDI_CLOCK;  
    static tooltip DIMREDUCT_AUDIO_SAMPLE_RATE;
    static tooltip DIMREDUCT_AUDIO_LENGTH;
    static tooltip DIMREDUCT_STFT;
    static tooltip DIMREDUCT_STFT_WINDOW_SIZE;
    static tooltip DIMREDUCT_STFT_HOP_SIZE;
    static tooltip DIMREDUCT_PCA;
    static tooltip DIMREDUCT_PCA_COMPONENTS;
    static tooltip DIMREDUCT_TSNE;
    static tooltip DIMREDUCT_TSNE_PERPLEXITY;
    static tooltip DIMREDUCT_TSNE_THETA;
    static tooltip DIMREDUCT_AUDIO_FEATURES;
    static tooltip OSC_RECEIVE_PORT;
    static tooltip OSC_RECEIVE_ADDRESS;
    static tooltip OSC_SEND_PORT;
    static tooltip OSC_SEND_ADDRESS;
    
};

