#include "Gui.h"

Gui* Gui::instance = 0;

Gui::Gui(Sounds * sounds, Modes * modes, Session * session) {
    this->sounds = sounds;
    this->modes = modes;
    this->session = session;
    this->tooltip = new Tooltip();


    font.load( OF_TTF_MONO, 10 );
    
    gui.setup(new GuiTheme(), true);
    drawGui = true;
}

Gui* Gui::getInstance(){
    /* Esto puede fallar??
    if(instance == 0){
        instance = new Gui();
    }
    */
    return instance;
}

Gui* Gui::getInstance(Sounds * sounds,
                      Modes * modes,
                      Session * session){

    if(instance == 0){
        instance = new Gui(sounds, modes, session);
    }

    return instance;
}


void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
    isDimReductScreenOpen = true;
}


void Gui::draw() {

    gui.begin();

    //Menu Superior
    drawMainMenu();

    if(drawGui) {




        Tooltip::drawGui();

        drawProcessingFilesWindow();

        drawAboutScreen();
        
        drawDimReductScreen();

        drawToolsWindow();

        drawClusterWindow();

        haveToHideCursor ? ofHideCursor() : ofShowCursor();

        if(toggleFullscreen
                && !isFullscreen
                || !toggleFullscreen
                && isFullscreen){

            ofToggleFullscreen();
            isFullscreen = !isFullscreen;
        }

    }
    if(!session->getDatasetLoaded()){
        drawWelcomeScreen();
    }

    drawFPS();
    drawMessage();

    gui.end();
}

void Gui::drawProcessingFilesWindow(){
    if ( isProcessingFiles ) {
        adr.checkProcess();

        if ( adr.progress == 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                //sessionManager->loadSession(resultJSONpath.c_str());

                adr.dirPath = "";

                string strSuccess = "Adjust clustering parameters using the Settings menu.\n";

                if ( !adr.failFiles.empty() ) {
                    strSuccess += "\n:( Some files couldn't be processed: ";
                    for ( auto s : adr.failFiles ) {
                        strSuccess += "\n" + s;
                    }
                }

                showMessage( strSuccess, "Process ended successfully" );
            } else {
                showMessage( "Unexpected error", "Error" );
            }
            AudioDimensionalityReduction::end();
        } else if ( adr.progress == -1.0f ) {
            isProcessingFiles = false;
            showMessage( adr.currentProcess, "Error" );
            AudioDimensionalityReduction::end();
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100));

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {

                isProcessingFilesWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_ChildWindows);

                ImGui::Text("Grab a coffee, "
                            "this can take quite a while.");

                ImGui::ProgressBar( adr.progress );
                ImGui::Text( adr.currentProcess.c_str() );
            }
            ImGui::End();
        }
    }
}

void Gui::drawToolsWindow(){
    //Settings
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    auto mainSettings = ofxImGui::Settings();

    mainSettings.windowPos = ImVec2(
                ofGetWidth() - WINDOW_TOOLS_WIDTH - 20,
                30);

    mainSettings.lockPosition = true;
    //Handle alpha
    if ( isToolsWindowHovered ){
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
    }else{
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
    }


    if( ofxImGui::BeginWindow("Tools", mainSettings, window_flags)){

            ImGui::SetWindowSize(
                        ImVec2(WINDOW_TOOLS_WIDTH,WINDOW_TOOLS_HEIGHT) );

            isToolsWindowHovered = ImGui::IsWindowHovered(
                        ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                        | ImGuiHoveredFlags_RootAndChildWindows);

            ImGui::SliderFloat(
                        "Master Volume",
                        &sounds->volumen,
                        0.0f,
                        1.0f);

            modes->drawModesSettings();
    }

    ofxImGui::EndWindow(mainSettings);
    ImGui::PopStyleVar();

}

bool Gui::drawWelcomeScreen(){

    ImGuiWindowFlags window_flags = 0;
    int w = 400;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
    
    ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));

    bool btn = false;

   if(isWelcomeScreenOpen){

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin("Audiostellar - Dataset Doctor" ,
                        &isWelcomeScreenOpen,
                        window_flags)){
           
           isWelcomeScreenHovered = ImGui::IsWindowHovered(
                       ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                       | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                       | ImGuiHoveredFlags_ChildWindows);

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(welcomeScreenText.c_str());
           ImGui::Columns(2,NULL, false);

           if(ImGui::Button("New Dataset")) {
              isWelcomeScreenOpen = false;
              newSession();
           }

           ImGui::NextColumn();

           if(ImGui::Button("Load Dataset")){
                isWelcomeScreenHovered = false;
                session->loadDataset();
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();

   } else {
       isWelcomeScreenHovered = false;
   }


   return btn;
}

void Gui::drawMainMenu(){

    if(ImGui::BeginMainMenuBar()){
        
        isMainMenuHovered = ImGui::IsWindowHovered(
                    ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                    | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                    | ImGuiHoveredFlags_ChildWindows);

        if(ImGui::BeginMenu("File")){

           if ( ImGui::MenuItem("New") ) { //"Ctrl+n"
               newSession();
           }

           if(ImGui::MenuItem("Add dataset")){ //"Ctrl+o"
               session->loadDataset();
           }

           if(ImGui::MenuItem("Close")){ //"Ctrl+q"
              exit(0);
           }

           ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &sounds->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show FPS", &haveToDrawFPS);
            ImGui::Checkbox("Hide Cursor", &haveToHideCursor);
            ImGui::Checkbox("Fullscreen", &toggleFullscreen);
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){
           sounds->drawGui();
           ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
                isAboutScreenOpen = true;
            }
           ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

}
void Gui::drawClusterWindow(){
    //Settings
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    auto mainSettings = ofxImGui::Settings();

    mainSettings.windowPos = ImVec2(
                0,
                30);

    mainSettings.lockPosition = true;
    //Handle alpha
    if ( isClusterWindowHovered ){
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
    }else{
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
    }

    if(ofxImGui::BeginWindow("Clusters", mainSettings, window_flags)){

            ImGui::SetWindowSize(
                        ImVec2(WINDOW_TOOLS_WIDTH,WINDOW_TOOLS_HEIGHT) );

            isClusterWindowHovered = ImGui::IsWindowHovered(
                        ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                        | ImGuiHoveredFlags_RootAndChildWindows);

    }

        set<int> clusterIds = sounds->getClusterIds();
        int nCols = 8;
        int n = 0;
        for (std::set<int>::iterator it = clusterIds.begin();
             it != clusterIds.end();
             ++it) {

            string clusterId = to_string(*it);
            if(n % nCols != 0){
                ImGui::SameLine();
            }
            if(ImGui::Button(clusterId.c_str())){
                sounds->onClusterToggle(*it);
            }
            n++;

        }



    ofxImGui::EndWindow(mainSettings);
    ImGui::PopStyleVar();

}

void Gui::drawAboutScreen(){

    if(haveToDrawAboutScreen) {
        if(isAboutScreenOpen){
            
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            
            ImGui::SetNextWindowPos(
                        ImVec2(ofGetWidth()/2 - 200,
                               ofGetHeight()/3));

            if(ImGui::Begin("About", &isAboutScreenOpen, window_flags)){
                
                isAboutScreenHovered = ImGui::IsWindowHovered(
                            ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                            | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                            | ImGuiHoveredFlags_ChildWindows);
                
               ImGui::Text("Thanks for using AudioStellar v0.8.0. \n\n\nMore info at https://gitlab.com/ayrsd/audiostellar");

            }

            ImGui::End();

        }else{
            haveToDrawAboutScreen = false;
            isAboutScreenHovered = false;
        }
    }
}

void Gui::drawDimReductScreen(){
    
    if(haveToDrawDimReductScreen) {
        
        if(isDimReductScreenOpen){
            
            ImGuiWindowFlags window_flags = 0;
            int w = 400;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));
            
            if (ImGui::Begin("Generate new session" ,
                             &isDimReductScreenOpen,
                             window_flags)){
                
                isDimReductScreenHovered = ImGui::IsWindowHovered(
                            ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                            | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                            | ImGuiHoveredFlags_ChildWindows);
                
                //path
                ImGui::Text("Folder location");
                
                string folder;
                string containingFolder;
                string displayFolder;
                
                if(adr.dirPath.size()) {
                    folder = ofFilePath::getPathForDirectory(adr.dirPath);
                    displayFolder = folder;
                }
                
                ImGui::PushStyleVar(
                            ImGuiStyleVar_WindowPadding,
                            ImVec2(0,3));

                ImGui::BeginChild("child", ImVec2(208, 19), true);
                ImGui::TextUnformatted( displayFolder.c_str() );
                ImGui::EndChild();
                ImGui::PopStyleVar();
                
                ImGui::SameLine();
                if(ImGui::Button("Select folder")){
                    ofFileDialogResult dialogResult = ofSystemLoadDialog(
                                "Select Folder",
                                true,
                                ofFilePath::getUserHomeDir());

                    string path;

                    if(dialogResult.bSuccess) {
                        adr.dirPath = dialogResult.getPath();

                    }
                }
                ImGui::Text("");
                
//                ImGui::Text("Audio");
//                ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
//                adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);
                
                ImGui::InputFloat("Target audio length", &adr.targetDuration);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_LENGTH);

                ImGui::Combo("Audio feature extraction", &features_item_current, features_items, IM_ARRAYSIZE(features_items));
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_FEATURES);
                
//                ImGui::Text("STFT");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT);
//                ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
//                adr.stft_windowSize = window_size_values[window_size_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);
                
//                ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
//                adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
//                ImGui::Text("");
                
//                ImGui::Text("PCA");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA);
//                ImGui::InputInt("PCA components", &adr.pca_nComponents);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA_COMPONENTS);
//                ImGui::Text("");
                
//                ImGui::Text("T-SNE");
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE);
//                ImGui::InputDouble("Perplexity", &adr.tsne_perplexity);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);
//                ImGui::InputDouble("Theta", &adr.tsne_theta);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_THETA);
//                ImGui::Text("");
                
                
                if(adr.dirPath.size()) {
                    if(ImGui::Button("Run")) {
                        adr.audioFeatures = features_items[features_item_current];

                        haveToDrawDimReductScreen = false;
                        isDimReductScreenHovered = false;
                        isProcessingFiles = true;
                        isWelcomeScreenOpen = false;
                        adr.startThread();
                    }
                }else{
                    ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                    ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                    ImGui::PushStyleColor(ImGuiCol_Text, textColor);
                    ImGui::PushStyleColor(ImGuiCol_Button, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                    ImGui::Button("Run");
                    ImGui::PopStyleColor(4);
                }
                ImGui::End();
            }
            ImGui::PopStyleVar();
            
        }else {
            isDimReductScreenHovered = false;
        }
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::drawMessage()
{
    if ( haveToDrawMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 350;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2,
                                        (ofGetHeight()/2) - h/2 ));

        if(ImGui::Begin( messageTitle.c_str() ,
                         &haveToDrawMessage,
                         window_flags)) {
            
            isMessageWindowHovered = ImGui::IsWindowHovered(
                        ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                        | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                        | ImGuiHoveredFlags_ChildWindows);
            
           ImGui::PushTextWrapPos(w);
           ImGui::Text(messageDescription.c_str());
        }
        ImGui::End();
    } else {
        isMessageWindowHovered = false;
    }
}

void Gui::showMessage(string description, string title, bool showCloseButton)
{
    haveToDrawMessage = true;
    haveToDrawMessageCloseButton = showCloseButton;
    messageTitle = title;
    messageDescription = description;
}

void Gui::hideMessage()
{
    haveToDrawMessage = false;
}

void Gui::keyPressed(int key) {
    if(key == 'g') {
        drawGui = !drawGui;
    }
}

bool Gui::isMouseHoveringGUI() {
    
    return (isToolsWindowHovered
            || isMainMenuHovered
            || isModeSelectorHovered
            || isAboutScreenHovered
            || isWelcomeScreenHovered
            || isDimReductScreenHovered
            || isProcessingFilesWindowHovered
            || isMessageWindowHovered);
}

