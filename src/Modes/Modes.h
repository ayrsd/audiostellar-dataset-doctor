#pragma once

#include "ofMain.h"
#include "Sounds.h"
#include "ExplorerMode.h"
#include "Mode.h"
#include "Utils.h"
#include "ImageButton.h"
#include "ColorPaletteGenerator.h"
#include "ofxImGui.h"
#include "Tooltip.h"

class MidiServer;

class Modes {
private:
    Mode *atMode;

    Sounds *sounds = NULL;

public:
    vector<Mode*> modes;
    vector<string> modeNames;
    vector<Tooltip::tooltip> modeTooltips;


    Modes(Sounds *_sounds);
    void initModes();
    void beforeDraw();
    void draw();
    void drawModeSelector();
    void drawModesSettings();
    void update();
    void mousePressed(int x, int y, int button);
    void keyPressed(int key);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y , int button);
    void mouseMoved(int x, int y);
    void midiMessage(Utils::midiMsg m);
    Mode* getActiveMode();
    string getActiveModeName();
    
    bool isModeSelectorHovered = false;


};
