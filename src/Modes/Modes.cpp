#include "Modes.h"


Modes::Modes(Sounds *_sounds) {
    sounds = _sounds;

    initModes();
}

void Modes::initModes() {
    modes.push_back(new ExplorerMode(sounds));

    for(unsigned int i = 0; i < modes.size(); i++) {
        modeNames.push_back(modes[i]->getModeName());
    }

    //seleccioná un modo inicial
    atMode = modes[0];
}

void Modes::beforeDraw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->beforeDraw();
    }
}
void Modes::draw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->draw();
    }
}

void Modes::update() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->update();
    }
}

void Modes::drawModesSettings(){
   for(int i = 0; i < modes.size(); i++){
       modes[i]->drawGui();
   }
}


void Modes::mousePressed(int x, int y, int button) {
    atMode->mousePressed( ofVec2f(x,y), button );
}

void Modes::keyPressed(int key) {
    atMode->keyPressed(key);
}

void Modes::mouseDragged(int x, int y, int button){
    atMode->mouseDragged(ofVec2f(x,y), button);
}

void Modes::mouseReleased(int x, int y , int button) {
    atMode->mouseReleased(ofVec2f(x,y));
}

void Modes::mouseMoved(int x, int y) {
    atMode->mouseMoved(ofVec2f(x,y));
}

Mode *Modes::getActiveMode()
{
    return atMode;
}

string Modes::getActiveModeName(){
    return atMode->getModeName();

}
