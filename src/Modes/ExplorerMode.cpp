#include "ExplorerMode.h"

ExplorerMode::ExplorerMode(Sounds *_sounds) {
    sounds = _sounds;
    sounds->hoveredActivated = true;
    modeName = "Explorer Mode";
    
}

void ExplorerMode::reset(){
}

void ExplorerMode::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = sounds->getHoveredSound();
    if ( hoveredSound != NULL ) {
        if ( button == 0 ) {
            sounds->playSound( hoveredSound );
        }
        
        if (button == 2) {
            /*
            showContextMenu = true;
            hoveredSoundPath = hoveredSound->getFileName();
            */
            sounds->onClusterToggle(hoveredSound->getCluster());

        }
        else showContextMenu = false;
    }
    else showContextMenu = false;

}

Json::Value ExplorerMode::save(){
    Json::Value root = Json::Value( Json::objectValue );
    return root;
}

void ExplorerMode::load(Json::Value jsonData){
}

void ExplorerMode::drawGui() {
    
    if (showContextMenu) {
        
        string buttonText;
        string command;
        
        #ifdef TARGET_WIN32
            buttonText = "Show in Explorer";
            command = "explorer /select," + hoveredSoundPath;
            cout << "dfgdf" << "\n";
        #endif
        
        #ifdef TARGET_OSX
            buttonText = "Show in Finder";
            command = "open -R " + hoveredSoundPath;
        #endif
        
        #ifdef TARGET_LINUX
            buttonText = "Show in File Manager";
            string hoveredSoundEnclosingDirectory = ofFilePath::getEnclosingDirectory(hoveredSoundPath);
            string script {
                "file_manager=$(xdg-mime query default inode/directory | sed 's/.desktop//g'); "
                "if [ \"$file_manager\" = \"nautilus\" ] || [ \"$file_manager\" = \"dolphin\" ] || [ \"$file_manager\" = \"nemo\" ]; "
                "then command=\"$file_manager %s\"; "
                "else command=\"xdg-open %s\"; "
                "fi; "
                "$command"
            };
            
            char buffer[ script.size() + hoveredSoundPath.size() + hoveredSoundEnclosingDirectory.size() ];
            std::sprintf( buffer, script.c_str(), hoveredSoundPath.c_str(), hoveredSoundEnclosingDirectory.c_str() );
            command = buffer;
        #endif
        
        ImGui::OpenPopup("menu");
        if (ImGui::BeginPopup("menu")) {
            ImGui::Text("File name");
            if (ImGui::Button(buttonText.c_str())) {               
                ofSystem(command);
                ImGui::CloseCurrentPopup();
                showContextMenu = false;
            }
            ImGui::EndPopup();
        }
    }
}
