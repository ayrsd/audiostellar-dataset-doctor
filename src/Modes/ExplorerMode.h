#pragma once

#include "ofMain.h"
#include "Mode.h"
#include "Sounds.h"

class ExplorerMode: virtual public Mode {

private:
    Sounds *sounds;

public:
    ExplorerMode(Sounds *_sounds);

    void reset();
    void mousePressed(ofVec2f p, int button);

    Json::Value save();
    void load( Json::Value jsonData );
    
    bool showContextMenu = false;
    string hoveredSoundPath;
    
    void drawGui();
};
