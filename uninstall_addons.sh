#!/bin/sh


PROJECT_DIR=$(dirname "$0")

OF_ROOT=$PROJECT_DIR/../../..

rm -r $PROJECT_DIR/bin/data/assets

rm -r $OF_ROOT/addons/ofxMidi
rm -r $OF_ROOT/addons/ofxSimpleTimer
rm -r $OF_ROOT/addons/ofxJSON
rm -r $OF_ROOT/addons/ofxConvexHull
rm -r $OF_ROOT/addons/ofxTweener
rm -r $OF_ROOT/addons/ofxImGui
rm -r $OF_ROOT/addons/ofxAudioFile
rm -r $OF_ROOT/addons/ofxFft
rm -r $OF_ROOT/addons/ofxPCA
rm -r $OF_ROOT/addons/ofxTSNE

echo "DONE"
