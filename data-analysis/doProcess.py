#!/usr/bin/env python
import argparse
import os
import utils
import numpy as np
import sys
from ProcessStatus import saveStatus


parser = argparse.ArgumentParser()
parser.add_argument("audioFilesPath", help="Path to audio files to parse")
parser.add_argument("-f", "--features", default = "stft", help="Features to extract from audio. stft or mfcc. Default: stft")
parser.add_argument("-n", "--n-files", default = "-1", help="Quantity of files to process. Default: all files found")
parser.add_argument("-l", "--audio-max-length", type=float, default = 3.0, help="Seconds. Longer audios will be chopped, shorter ones will be zero-padded. Only for processing, original audios will not be affected. Default: 3")

args = parser.parse_args()

if os.path.isdir(args.audioFilesPath):
    if args.audioFilesPath[-1] != "/":
        args.audioFilesPath += "/"
    print("reading directory", args.audioFilesPath, "...")
else:
    print("audioFilesPath not found :(")
    saveStatus("currentProcess", "Path not found")
    saveStatus("progress", -1)
    sys.exit()

audioFilesPathDirectoryName = os.path.basename(args.audioFilesPath[0:-1])

audioFiles = utils.findMusic( args.audioFilesPath )
saveStatus("qtyFiles",len(audioFiles))
saveStatus("progress", 0)
print(len(audioFiles), "files found")

if len(audioFiles) == 0:
    saveStatus("currentProcess", "No wav or mp3 files found")
    saveStatus("progress", -1)
    sys.exit()

args.n_files = int(args.n_files)
if args.n_files == -1:
    args.n_files = len(audioFiles)

print("now processing")
saveStatus("currentProcess","Extracting features")
audioData = utils.getAudioData( audioFiles, features = args.features, qtyFilesToProcess = args.n_files, audioMaxLength = args.audio_max_length )

saveStatus("qtyFiles",len(audioFiles))
if len(audioFiles) < 5:
    print("Less than 5 audiofiles were processed, aborting.")
    saveStatus("currentProcess", "Less than 5 audiofiles were processed")
    saveStatus("progress", -1)
    sys.exit()

print("DONE!")

print("")
print("Now doing PCA")
saveStatus("progress", 0.5)
saveStatus("currentProcess","Running principal component analysis")
audioDataTransformed = utils.doPCA(audioData)

saveStatus("progress", 0.7)
saveStatus("currentProcess","Measuring similarity between all sounds")
print("")
print("Now measuring similarity between all sounds")

utils.doDistanceMatrix( audioDataTransformed )

saveStatus("progress", 0.9)
saveStatus("currentProcess","Running t-SNE")
print("")
print("Now doing t-SNE")
tsne = utils.doTSNE( audioDataTransformed, 2 )
print("DONE!")

print("")
print("Now saving session file")

import json

jsonSession = {
    "audioFilesPath" : "%s/" % audioFilesPathDirectoryName,
    "tsv" : ""
}

audioFilesForExport = list( map( lambda x : x[len(args.audioFilesPath):], audioFiles ) )
# x , y , z , clusterNumber, filename
output = np.c_[ tsne, np.repeat(0, tsne.shape[0] ) , np.repeat(0, tsne.shape[0]), audioFilesForExport ]

tsv = ""
for row in output:
    for field in row:
        tsv += field + ","
    tsv = tsv[0:-1]
    tsv += "|"

jsonSession["tsv"] = tsv

filenameOk = False
currentFilenameNumber = 1
while ( not filenameOk ):
    newFilename = '%s.%s.%d.json' % ( audioFilesPathDirectoryName, args.features, currentFilenameNumber )
    newFilepath = os.path.join(args.audioFilesPath, "../", newFilename )
    newFilepath = os.path.realpath(newFilepath)

    if ( os.path.exists(newFilepath) ):
        currentFilenameNumber += 1
    else:
        filenameOk = True

with open( newFilepath, 'w') as fp:
    json.dump(jsonSession, fp)

saveStatus("currentProcess", "All done")
saveStatus("generatedJSONPath", newFilepath)
saveStatus("progress", 1)

print("All done, have a great day")
